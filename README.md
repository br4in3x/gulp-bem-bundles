# Gulp и сборка БЭМ-бандлов #

Этот репозиторий - пример того, как можно собрать бандлы по методологии БЭМ, используя Gulp.

[БЭМ](http://bem.info/ "БЭМ") [Gulp](http://gulpjs.com/ "Gulp") [BEM Bundle](https://ru.bem.info/methodology/declarations/ "BEM Bundle")

## Как использовать ##

```
#!bash

npm i
gulp <task> --bundle <bundle_name>
```
