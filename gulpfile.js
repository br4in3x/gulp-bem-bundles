/*jslint node: true*/
/*global require*/
'use strict';

var gulp      = require('gulp');
var bem       = require('@bem/gulp');
var merge     = require('gulp-merge');
var concat    = require('gulp-concat');
var sass      = require('gulp-sass');
var uglify    = require('gulp-uglify');
var argv      = require('yargs').argv;
var imagemin  = require('gulp-imagemin');
var pngquant  = require('imagemin-pngquant');
var watch     = require('gulp-watch');
var rename    = require('gulp-rename');

var levelConfig = {
    scheme: 'nested',
    naming: {
        elem: '__',
        mod: { name: '--', val: '_' },
        wordPattern: '[a-zA-Z0-9]+'
    }
};

var project = bem({
    bemconfig: {
        'blocks': levelConfig,
        'libs': levelConfig
    }
});

var config = {
    bundle: {
        name: argv.bundle || 'index'
    },
    paths: {
        bundle: 'bundle/' + (argv.bundle || 'index'),
        images: 'blocks/**/images/*.*',
        scss:   'blocks/**/*.scss',
        js:     'blocks/**/*.js',
        fonts:  'blocks/**/fonts/*.*'
    }
};

var bundle = project.bundle({
    path: config.paths.bundle,
    decl: config.bundle.name + '.bemdecl.js'
});

gulp.task('js', function () {
    return bundle.src({ tech: 'js', extensions: ['.js'] })
        .pipe(concat(config.bundle.name + '.js'))
        .pipe(uglify()) // Углифицирование :D
        .pipe(gulp.dest(config.paths.bundle));
});

gulp.task('image', function () {
    return gulp.src(config.paths.images)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest('media/images'));
});

gulp.task('fonts', function () {
    gulp.src(config.paths.fonts)
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(config.paths.bundle + 'media/fonts'));
});

gulp.task('css', function () {
    return bundle.src({ tech: 'css', extensions: ['.scss', '.css'] })
        .pipe(concat(config.bundle.name + '.scss'))
        .pipe(sass({ 
            outputStyle: 'expanded', 
            includePaths: ['./libs'] 
        }).on('error', sass.logError))
        .pipe(gulp.dest(config.paths.bundle));
});

gulp.task('watch', function () {
    watch(config.paths.scss, function () {
        gulp.start('css');
    });
    watch([config.paths.js], function () {
        gulp.start('js');
    });
    watch([config.paths.images], function () {
        gulp.start('image');
    });
    watch([config.paths.fonts], function () {
        gulp.start('fonts');
    });
});

gulp.task('default', ['css', 'js', 'image', 'fonts']);
